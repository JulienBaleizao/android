package musicaround.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import musicaround.common.helpers.JsonHelper;
import musicaround.model.Lounge;
import org.bson.types.ObjectId;

public class Test {

    @org.junit.Test
    public void test() {
        Lounge lounge = Lounge.builder()._id(ObjectId.get()).build();
        try {
            System.out.println(JsonHelper.OBJECT_MAPPER.writeValueAsString(lounge));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
