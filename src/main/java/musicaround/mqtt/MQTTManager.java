package musicaround.mqtt;

import lombok.extern.slf4j.Slf4j;
import musicaround.config.YAMLConfig;
import org.eclipse.paho.client.mqttv3.*;

import java.util.Base64;
import java.util.UUID;

@Slf4j
public class MQTTManager {
    
    public static final String TOPIC_PREFIX_LOUNGE = "lounge";

    private YAMLConfig config;

    private IMqttClient mqttClient;

    public MQTTManager(YAMLConfig config) {
        this.config = config;
    }

    public MQTTManager connect() {
        log.info("Connecting to " + config.getMqttServerUrl());

        try {
            mqttClient = new MqttClient(config.getMqttServerUrl(), MqttAsyncClient.generateClientId());
        } catch (MqttException e) {
            log.error("Fail to instantiate MQTT client", e);
            return this;
        }

        try {
            mqttClient.connect(getMqttConectOptions());
        } catch (MqttException e) {
            log.error("Fail to connect to MQTT server at " + config.getMqttServerUrl(), e);
            return this;
        }

        log.info("Connected to " + config.getMqttServerUrl());
        return this;
    }

    public boolean isConnected() {
        if (this.mqttClient == null) {
            return false;
        }
        return this.mqttClient.isConnected();
    }

    public void disconnect() {
        if (isConnected()) {
            log.info("Disconnecting from " + config.getMqttServerUrl());
            try {
                mqttClient.disconnect();
            } catch (MqttException e) {
                log.error("Fail to disconnect to MQTT server at " + config.getMqttServerUrl(), e);
            }
            log.info("Disconnected from " + config.getMqttServerUrl());
        } else {
            log.info("Already disconnected from " + config.getMqttServerUrl());
        }
    }

    public String generateTopicForLounge() {
        return TOPIC_PREFIX_LOUNGE + "/" + new String(Base64.getUrlEncoder().encode(UUID.randomUUID().toString().getBytes())).substring(0, 15);
    }

    public void publish(String topic, String payload) throws MqttException {
        if (isConnected()) {
            mqttClient.publish(topic, payload.getBytes(), 0, false);
        }
    }

    private MqttConnectOptions getMqttConectOptions() {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        return options;
    }
}
