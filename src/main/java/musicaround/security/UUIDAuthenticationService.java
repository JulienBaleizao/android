package musicaround.security;

import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import musicaround.model.Token;
import musicaround.model.User;
import musicaround.service.TokensService;
import musicaround.service.UsersService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@Service
@AllArgsConstructor(access = PACKAGE)
@FieldDefaults(level = PRIVATE, makeFinal = true)
@Slf4j
final class UUIDAuthenticationService implements UserAuthenticationService {

    @Autowired
    UsersService usersService;
    @Autowired
    TokenHelper tokenHelper;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Optional<Token> login(final String email, final String password) {
        Optional<User> user = usersService.findByEmail(email);

        if (!user.isPresent()) {
            return Optional.empty();
        }

        if (!passwordEncoder.matches(password, user.get().getPassword())) {
            log.debug("Password not matching");
            return Optional.empty();
        }

        Token token = tokenHelper.generateToken(user.get());

        return Optional.of(token);
    }

    @Override
    public Optional<User> findByToken(final String t) {
        Optional<Token> token = tokenHelper.findByToken(t);

        if (!token.isPresent()) {
            return Optional.empty();
        }

        if (!token.get().isValid()) {
            log.debug("Token not valid");
            return Optional.empty();
        }

        Optional<User> user = usersService.findBy_id(new ObjectId(token.get().getUserId()));

        if (!user.isPresent()) {
            return Optional.empty();
        }

        return user;
    }

    @Override
    public void logout(final User u) {
        tokenHelper.invalidateTokenForUser(u);
    }

    @Override
    public void logout(final String token) {
        tokenHelper.invalidateToken(token);
    }
}