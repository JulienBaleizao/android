package musicaround.security;

import musicaround.model.Token;
import musicaround.model.User;
import musicaround.service.TokensService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class TokenHelper {

    @Autowired
    TokensService tokensService;

    public Token generateToken(User user) {
        Token token = Token.builder()
                .token(UUID.randomUUID().toString())
                .refreshToken(UUID.randomUUID().toString())
                .expireIn(Long.MAX_VALUE)
                .userId(user.get_id())
                .valid(true).build();
        tokensService.save(token);
        return token;
    }

    public Optional<Token> findByToken(String t) {
        return tokensService.findByToken(t);
    }

    public Optional<Token> findValidByUserId(String userId) {
        return tokensService.findValidByUserId(userId);
    }

    public void invalidateTokenForUser(User u) {
        List<Token> tokens = tokensService.findByUserId(u.get_id());

        tokensService.deleteAll(tokens);
    }

    public void invalidateToken(String token) {
        Optional<Token> tokenOpt = tokensService.findByToken(token);

        tokenOpt.ifPresent(t -> tokensService.delete(t));
    }
}
