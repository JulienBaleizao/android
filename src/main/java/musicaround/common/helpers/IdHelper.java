package musicaround.common.helpers;

import java.util.Base64;
import java.util.UUID;

public class IdHelper {

    public static String generateEncodedUrlId() {
        return new String(Base64.getUrlEncoder().encode(UUID.randomUUID().toString().getBytes()));
    }
}
