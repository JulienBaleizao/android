package musicaround.common.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonHelper {

    public static final Gson GSON = new Gson();
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
}
