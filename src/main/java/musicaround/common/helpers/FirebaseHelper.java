package musicaround.common.helpers;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import lombok.extern.slf4j.Slf4j;
import musicaround.config.YAMLConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.util.List;
import java.util.Map;

@Slf4j
public class FirebaseHelper {

    private YAMLConfig config;
    private FirebaseApp firebaseApp;

    public FirebaseHelper() throws IOException {
    }

    public FirebaseHelper init(YAMLConfig config) throws IOException {
        this.config = config;
        NetHttpTransport.Builder netHttpTransportBuilder = new NetHttpTransport.Builder();
        if (config.isProxyEnabled()) {
            SocketAddress addr = new
                    InetSocketAddress(config.getProxyHost(), config.getProxyPort());
            netHttpTransportBuilder.setProxy(new Proxy(Proxy.Type.HTTP, addr));
        }

        InputStream inputStream = new ClassPathResource("musicaround-1537798829304-firebase-adminsdk-ktwth-55896af1d6.json").getInputStream();
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(inputStream))
                .setDatabaseUrl("https://musicaround-1537798829304.firebaseio.com")
                .setHttpTransport(netHttpTransportBuilder.build())
                .build();

        firebaseApp = FirebaseApp.initializeApp(options);

        return this;
    }

    public void sendMessage(String registrationToken, Map<String, String> allData, String notificationTitle, String notificationBody, List<String> bodyArgs, AndroidConfig.Priority priority, String tag, String collapseKey) throws FirebaseMessagingException {
        AndroidConfig.Builder androidConfigBuilder = AndroidConfig.builder().setPriority(priority);

        if (!StringUtils.isBlank(collapseKey)) {
            androidConfigBuilder.setCollapseKey(collapseKey);
        }

        androidConfigBuilder.setNotification(AndroidNotification.builder()
                .setBodyLocalizationKey("FIREBASE_LOUNGE_CREATED_NOTIF_BODY")
                .setTitleLocalizationKey("FIREBASE_LOUNGE_CREATED_NOTIF_TITLE")
                .addAllBodyLocalizationArgs(bodyArgs)
                .setTag(tag)
                .setIcon("ic_share_black_24dp")
                .build());

        // See documentation on defining a message payload.
        Message message = Message.builder()
                .putAllData(allData)
                //.setNotification(new Notification(notificationTitle, notificationBody))
                .setToken(registrationToken)
                .setAndroidConfig(androidConfigBuilder.build())
                .build();

        String response = null;

        log.debug("Sending message: " + message.toString());
        response = FirebaseMessaging.getInstance().send(message);

        // Response is a message ID string.
        log.debug("Firebase message sent: " + response);
    }
}
