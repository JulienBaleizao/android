package musicaround.common;

public class ErrorCode {

    public static final String USER_EMAIL_ALREADY_EXISTS = "100";
    public static final String USER_USERNAME_ALREADY_EXISTS = "101";
    public static final String USER_EMAIL_NOT_VALID = "102";
}
