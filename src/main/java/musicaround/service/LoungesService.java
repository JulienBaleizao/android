package musicaround.service;

import musicaround.controller.LoungeController;
import musicaround.model.Lounge;
import musicaround.repositories.LoungesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LoungesService {

    @Autowired
    LoungesRepository loungesRepository;

    public void save(Lounge lounge) {
        loungesRepository.save(lounge);
    }

    public Optional<Lounge> findById(String id) {
        return loungesRepository.findById(id);
    }

    public Page<Lounge> findAll(Pageable pageable) {
        return loungesRepository.findAll(pageable);
    }

    public List<Lounge> findExpiredLounge(Pageable pageable) {
        return loungesRepository.findByLastUpdateIsLessThan(System.currentTimeMillis() - LoungeController.EXPIRED_DELAY_MS, pageable).getContent();
    }

    public List<Lounge> deleteExpiredLounge() {
        List<Lounge> lounges = findExpiredLounge(PageRequest.of(0, 100));
        loungesRepository.deleteAll(lounges);
        return lounges;
    }

    public Page<Lounge> findAllOrderByCreationDateAsc(Pageable pageable) {
        return loungesRepository.findAllByOrderByCreationDateDesc(pageable);
    }

    public Page<Lounge> findAllByOrderByNbListeningDesc(Pageable pageable) {
        return loungesRepository.findAllByOrderByNbListeningDesc(pageable);
    }

    public void deleteAll() {
        loungesRepository.deleteAll();
    }

    public Optional<Lounge> findByUserInternalId(String userInternalId) {
        return loungesRepository.findByUserInternalId(userInternalId);
    }

    public List<Lounge> findByUsernameIsLike(String username, Pageable pageable) {
        return loungesRepository.findByUserUsernameIsLikeIgnoreCase(username, pageable);
    }

    public void deleteById(String id) {
        loungesRepository.deleteById(id);
    }
}
