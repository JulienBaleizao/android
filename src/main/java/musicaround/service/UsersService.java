package musicaround.service;

import musicaround.model.User;
import musicaround.repositories.UsersRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsersService {

    @Autowired
    UsersRepository usersRepository;

    public Optional<User> findByUsername(String username) {
        return usersRepository.findByUsername(username);
    }

    public Optional<User> findByUsernameIgnoreCase(String username) {
        return usersRepository.findByUsernameIgnoreCase(username);
    }

    public List<User> findByUsernameIsLike(String usernameStarts, Pageable pageable) {
        return usersRepository.findByUsernameIsLikeIgnoreCase(usernameStarts, pageable);
    }

    public List<User> findByInternalIds(List<String> internalIds, Pageable pageable) {
        return usersRepository.findByInternalIdIn(internalIds, pageable);
    }

    public void save(User user) {
        usersRepository.save(user);
    }

    public Page<User> findAll(Pageable pageable) {
        return usersRepository.findAll(pageable);
    }

    public void deleteAll() {
        usersRepository.deleteAll();
    }

    public Optional<User> findBy_id(ObjectId objectId) {
        return usersRepository.findBy_id(objectId);
    }

    public Optional<User> findByInternalId(String id) {
        return usersRepository.findByInternalId(id);
    }

    public Optional<User> findByEmail(String email) {
        return usersRepository.findByEmail(email);
    }
}
