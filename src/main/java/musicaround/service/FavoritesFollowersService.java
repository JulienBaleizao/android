package musicaround.service;

import musicaround.model.FavoritesFollowers;
import musicaround.repositories.FavoritesFollowersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FavoritesFollowersService {

    @Autowired
    FavoritesFollowersRepository favoritesFollowersRepository;

    public void save(FavoritesFollowers favoritesFollowers) {
        favoritesFollowersRepository.save(favoritesFollowers);
    }

    public Page<FavoritesFollowers> findAll(Pageable pageable) {
        return favoritesFollowersRepository.findAll(pageable);
    }

    public void deleteAll() {
        favoritesFollowersRepository.deleteAll();
    }

    public Optional<FavoritesFollowers> findByUserFollowed(String userInternalId) {
        return favoritesFollowersRepository.findByUserFollowed(userInternalId);
    }

    public void deleteById(String id) {
        favoritesFollowersRepository.deleteById(id);
    }
}
