package musicaround.service;

import musicaround.model.Token;
import musicaround.repositories.TokensRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TokensService {

    @Autowired
    TokensRepository tokensRepository;

    public void delete(Token token) {
        tokensRepository.delete(token);
    }

    public void deleteById(String id) {
        tokensRepository.deleteById(id);
    }

    public void deleteAll(List<Token> tokens) {
        tokensRepository.deleteAll(tokens);
    }

    public Optional<Token> findByToken(String t) {
        return tokensRepository.findByToken(t);
    }

    public void save(Token token) {
        tokensRepository.save(token);
    }

    public List<Token> findByUserId(String userId) {
        return tokensRepository.findByUserId(userId);
    }

    public Optional<Token> findValidByUserId(String userId) {
        return tokensRepository.findByUserIdAndValid(userId, true);
    }

    public Page<Token> findAll(Pageable pageable) {
        return tokensRepository.findAll(pageable);
    }

    public void deleteAll() {
        tokensRepository.deleteAll();
    }
}
