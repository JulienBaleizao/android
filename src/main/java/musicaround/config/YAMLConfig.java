package musicaround.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties
@ConfigurationProperties
public class YAMLConfig {

    @Value("${mqtt.server.url}")
    private String mqttServerUrl;

    @Value("${server.proxy.host}")
    private String proxyHost;

    @Value("${server.proxy.port}")
    private String proxyPort;

    public String getMqttServerUrl() {
        return mqttServerUrl;
    }

    public void setMqttServerUrl(String mqttServerUrl) {
        this.mqttServerUrl = mqttServerUrl;
    }

    public boolean isProxyEnabled() {
        return !StringUtils.isBlank(proxyHost) && !StringUtils.isBlank(proxyPort);
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return Integer.valueOf(proxyPort);
    }
}
