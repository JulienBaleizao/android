package musicaround.lounge.model;

import musicaround.common.helpers.JsonHelper;

public class Message {

    private long sentDate = System.currentTimeMillis();
    private MessageType messageType;
    private Endpoint endpoint;

    public Message(MessageType messageType) {
        this.messageType = messageType;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }

    public long getSentDate() {
        return sentDate;
    }

    public void setSentDate(long sentDate) {
        this.sentDate = sentDate;
    }

    public String buildPayload() {
        return JsonHelper.GSON.toJson(this);
    }

    public boolean isMessageType(MessageType messageType) {
        return this.getMessageType().compareTo(messageType) == 0;
    }
}