package musicaround.lounge.model;

public class CloseMessage extends Message {

    public CloseMessage() {
        super(MessageType.CLOSE);
    }
}
