package musicaround.lounge;

import java.util.Base64;
import java.util.UUID;

public class LoungeHelper {

    public static String generateToken() {
        return new String(Base64.getUrlEncoder().encode(UUID.randomUUID().toString().getBytes())).substring(0, 15);
    }
}
