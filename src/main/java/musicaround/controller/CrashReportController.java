package musicaround.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.FirebaseMessagingException;
import lombok.extern.slf4j.Slf4j;
import musicaround.common.helpers.FirebaseHelper;
import musicaround.common.helpers.JsonHelper;
import musicaround.common.helpers.NotificationType;
import musicaround.exceptions.http.BadRequestException;
import musicaround.exceptions.http.NoContentException;
import musicaround.lounge.LoungeHelper;
import musicaround.lounge.model.CloseMessage;
import musicaround.model.FirebaseData;
import musicaround.model.Lounge;
import musicaround.model.User;
import musicaround.mqtt.MQTTManager;
import musicaround.service.FavoritesFollowersService;
import musicaround.service.LoungesService;
import musicaround.service.UsersService;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@RestController
@RequestMapping("/public/v1/report")
@Slf4j
public class CrashReportController {


    @RequestMapping(value = "", method = RequestMethod.POST)
    public void receivedReport(@AuthenticationPrincipal final User user, @RequestBody String report) {

        JSONObject reportJson;
        try {
            reportJson = new JSONObject(report);
        } catch (JSONException e) {
            log.error("Fail to convert report to JSON", e);
            return;
        }

        log.debug("Report received: \n" + reportJson);
    }
}