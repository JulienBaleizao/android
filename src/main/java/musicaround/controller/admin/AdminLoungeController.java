package musicaround.controller.admin;

import lombok.extern.slf4j.Slf4j;
import musicaround.model.Lounge;
import musicaround.model.User;
import musicaround.service.LoungesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("admin/v1/lounge")
@Slf4j
public class AdminLoungeController {

    @Autowired
    private LoungesService loungesService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Lounge> findAll(@AuthenticationPrincipal final User user, @RequestParam("page") int page, @RequestParam("size") int size) {


        Page<Lounge> loungePage = loungesService.findAll(PageRequest.of(page, size));

        return loungePage.getContent();
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public void deleteAll(@AuthenticationPrincipal final User user) {
        loungesService.deleteAll();
    }
}