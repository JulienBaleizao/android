package musicaround.controller.admin;

import lombok.extern.slf4j.Slf4j;
import musicaround.model.FavoritesFollowers;
import musicaround.model.User;
import musicaround.service.FavoritesFollowersService;
import musicaround.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("admin/v1/user")
@Slf4j
public class AdminUserController {

    @Autowired
    private UsersService usersService;
    @Autowired
    private FavoritesFollowersService favoritesFollowersService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<User> getAllUsers(@RequestParam("page") int page, @RequestParam("size") int size) {
        return usersService.findAll(PageRequest.of(page, size)).getContent();
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public void deleteAllUsers() {
        usersService.deleteAll();
    }

    @RequestMapping(value = "favoritesFollowers", method = RequestMethod.GET)
    public List<FavoritesFollowers> getAllFavoritesFollowers(@AuthenticationPrincipal final User user, @RequestParam("page") int page, @RequestParam("size") int size) {
        return favoritesFollowersService.findAll(PageRequest.of(page, size)).getContent();
    }

    @RequestMapping(value = "favoritesFollowers", method = RequestMethod.DELETE)
    public void deleteAllFavoritesFollowers(@AuthenticationPrincipal final User user) {
        favoritesFollowersService.deleteAll();
    }
}