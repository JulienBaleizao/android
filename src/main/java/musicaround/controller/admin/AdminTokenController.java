package musicaround.controller.admin;

import lombok.extern.slf4j.Slf4j;
import musicaround.model.Token;
import musicaround.model.User;
import musicaround.service.TokensService;
import musicaround.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("admin/v1/token")
@Slf4j
public class AdminTokenController {

    @Autowired
    private TokensService tokensService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Token> getAllTokens(@RequestParam("page") int page, @RequestParam("size") int size) {
        return tokensService.findAll(PageRequest.of(page, size)).getContent();
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public void deleteAllTokens() {
        tokensService.deleteAll();
    }
}