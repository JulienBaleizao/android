package musicaround.controller.admin;

import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.FirebaseMessagingException;
import lombok.extern.slf4j.Slf4j;
import musicaround.common.helpers.FirebaseHelper;
import musicaround.exceptions.http.BadRequestException;
import musicaround.model.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("admin/v1/firebase")
@Slf4j
public class AdminFirebaseController {

    @Autowired
    private FirebaseHelper firebaseHelper;

    @RequestMapping(value = "send", method = RequestMethod.POST)
    public void send(@AuthenticationPrincipal final User user, @RequestParam("token") String token) {
        if (StringUtils.isBlank(token)) {
            throw new BadRequestException("Bad input");
        }

        Map<String, String> data = new HashMap<>();
        data.put("firebase", "test");
        try {
            firebaseHelper.sendMessage(token, data, "Firebase", "test", new ArrayList<>(), AndroidConfig.Priority.HIGH, null, null);
        } catch (FirebaseMessagingException e) {
            log.error("Fail to send Firebase message [" + e.getErrorCode() + "]", e);
            return;
        }
    }
}