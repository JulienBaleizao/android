package musicaround.controller;

import lombok.extern.slf4j.Slf4j;
import musicaround.common.ErrorCode;
import musicaround.exceptions.http.BadRequestException;
import musicaround.exceptions.http.ConflictException;
import musicaround.model.FavoritesFollowers;
import musicaround.model.User;
import musicaround.service.FavoritesFollowersService;
import musicaround.service.UsersService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/user")
@Slf4j
public class UserController {

    @Autowired
    private UsersService usersService;
    @Autowired
    private FavoritesFollowersService favoritesFollowersService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public User getUser(@AuthenticationPrincipal final User user) {
        return user;
    }

    @RequestMapping(value = "/favorites", method = RequestMethod.GET)
    public List<User> getUserFavorites(@AuthenticationPrincipal final User user, @RequestParam("page") int page, @RequestParam("size") int size) {
        List<User> result = new ArrayList<>();

        return usersService.findByInternalIds(user.getFavorites(), PageRequest.of(page, size));
    }

    @RequestMapping(value = "/criteria", method = RequestMethod.GET)
    public List<User> getUserByQuery(@AuthenticationPrincipal final User user, @RequestParam("usernameStartsBy") String usernameStartsBy, @RequestParam("page") int page, @RequestParam("size") int size) {
        List<User> result = new ArrayList<>();

        if (!StringUtils.isBlank(usernameStartsBy)) {
            result = usersService.findByUsernameIsLike(usernameStartsBy, PageRequest.of(page, size));
        }

        return result;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public User updateUser(@AuthenticationPrincipal final User user, @RequestBody User userToUpdate) {
        if (user.getEmail() != null && !user.getEmail().equalsIgnoreCase(userToUpdate.getEmail())) {
            Optional<User> userFound = usersService.findByEmail(userToUpdate.getEmail());
            if (userFound.isPresent()) {
                log.warn("Email already exists");
                throw new BadRequestException(ErrorCode.USER_EMAIL_ALREADY_EXISTS);
            }
        }

        if (userToUpdate.getUsername() != null && !userToUpdate.getUsername().equalsIgnoreCase(user.getUsername())) {
            Optional<User> userFound = usersService.findByUsernameIgnoreCase(userToUpdate.getUsername());
            if (userFound.isPresent()) {
                log.warn("Username already exists");
                throw new ConflictException(ErrorCode.USER_USERNAME_ALREADY_EXISTS);
            }
            user.setUsername(userToUpdate.getUsername());
        }

        user.setEmail(userToUpdate.getEmail());
        user.setFirstName(userToUpdate.getFirstName());
        user.setLastName(userToUpdate.getLastName());
        user.setCountryIsoCode(userToUpdate.getCountryIsoCode());

        updateFavoritesFollowers(user, userToUpdate);

        user.setFavorites(userToUpdate.getFavorites());
        user.setFirebaseDatas(userToUpdate.getFirebaseDatas());

        usersService.save(user);
        return user;
    }

    private void updateFavoritesFollowers(User user, User userToUpdate) {
        List<String> userToDelete = new ArrayList<>(user.getFavorites());
        userToDelete.removeAll(userToUpdate.getFavorites());

        FavoritesFollowers favoritesFollowers;
        for(String userFollowedId : userToUpdate.getFavorites()) {
            favoritesFollowers = favoritesFollowersService.findByUserFollowed(userFollowedId).orElse(FavoritesFollowers.builder().userFollowed(userFollowedId).build());
            favoritesFollowers.addUsersFollowers(user.getInternalId());
            favoritesFollowersService.save(favoritesFollowers);
        }
        Optional<FavoritesFollowers> favoritesFollowersOpt;
        for(String userNoMoreFollowedId : userToDelete) {
            favoritesFollowersOpt = favoritesFollowersService.findByUserFollowed(userNoMoreFollowedId);
            if (favoritesFollowersOpt.isPresent()) {
                favoritesFollowersOpt.get().removeUsersFollowers(user.getInternalId());
                favoritesFollowersService.save(favoritesFollowersOpt.get());
            }
        }
    }
}