package musicaround.controller;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import musicaround.common.AuthProvider;
import musicaround.common.ErrorCode;
import musicaround.common.helpers.IdHelper;
import musicaround.common.helpers.WebHelper;
import musicaround.config.YAMLConfig;
import musicaround.exceptions.http.BadRequestException;
import musicaround.exceptions.http.ConflictException;
import musicaround.exceptions.http.CreatedException;
import musicaround.exceptions.http.UnauthorizedException;
import musicaround.model.Token;
import musicaround.model.User;
import musicaround.security.TokenHelper;
import musicaround.security.UserAuthenticationService;
import musicaround.service.UsersService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Optional;

@RestController
@RequestMapping("/public/v1/auth")
@Slf4j
public class PublicAuthController {

    private static final String CLIENT_ID_GOOGLE = "959335141410-cjnq4fm9aprse0ouossejp28ns114a33.apps.googleusercontent.com";

    @Autowired
    private YAMLConfig config;

    @Autowired
    @NonNull
    private UserAuthenticationService authentication;
    @Autowired
    private UsersService usersService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private TokenHelper tokenHelper;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void register(
            @RequestBody User userToRegister) {

        if (!WebHelper.isEmailValid(userToRegister.getEmail())) {
            log.warn("Email is not valid");
            throw new BadRequestException(ErrorCode.USER_EMAIL_NOT_VALID);
        }

        Optional<User> user = usersService.findByUsername(userToRegister.getUsername());

        if (user.isPresent()) {
            log.debug("Username " + userToRegister.getUsername() + " already exists");
            throw new ConflictException(ErrorCode.USER_USERNAME_ALREADY_EXISTS);
        }

        user = usersService.findByEmail(userToRegister.getEmail());

        if (user.isPresent()) {
            log.debug("Email already exists");
            throw new ConflictException(ErrorCode.USER_EMAIL_ALREADY_EXISTS);
        }

        saveUser(User
                .builder()
                .internalId(IdHelper.generateEncodedUrlId())
                .username(userToRegister.getUsername())
                .password(userToRegister.getPassword())
                .email(userToRegister.getEmail())
                .build());

        throw new CreatedException();
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public Token login(@RequestParam("email") final String email,
                       @RequestParam("password") final String password) {
        return authentication
                .login(email, password)
                .orElseThrow(() -> new UnauthorizedException("invalid login and/or password"));
    }

    @RequestMapping(value = "/login/google", method = RequestMethod.GET)
    public Token loginGoogle(@RequestParam("token") final String token) {
        NetHttpTransport.Builder netHttpTransportBuilder = new NetHttpTransport.Builder();
        if (config.isProxyEnabled()) {
            SocketAddress addr = new
                    InetSocketAddress(config.getProxyHost(), config.getProxyPort());
            netHttpTransportBuilder.setProxy(new Proxy(Proxy.Type.HTTP, addr));
        }
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(netHttpTransportBuilder.build(), JacksonFactory.getDefaultInstance())
                // Specify the CLIENT_ID of the app that accesses the backend:
                .setAudience(Collections.singletonList(CLIENT_ID_GOOGLE))
                // Or, if multiple clients access the backend:
                //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
                .build();

        GoogleIdToken idToken = null;
        try {
            idToken = verifier.verify(token);
        } catch (GeneralSecurityException | IOException e) {
            log.warn("Failed to verify Google token", e);
            throw new BadRequestException("invalid token");
        }

        if (idToken == null) {
            log.warn("Failed to verify Google token");
            throw new BadRequestException("invalid token");
        }

        Payload payload = idToken.getPayload();

        String userId = payload.getSubject();

        // Get profile information from payload
        String email = payload.getEmail();
        boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
        String name = (String) payload.get("name");
        String pictureUrl = (String) payload.get("picture");
        String locale = (String) payload.get("locale");
        String familyName = (String) payload.get("family_name");
        String givenName = (String) payload.get("given_name");

        log.debug("Google Id Token payload: " + payload.toString());

        Optional<User> user = usersService.findByInternalId(userId);

        if (!user.isPresent()) {
            log.debug("Creating user with ID " + userId + " from " + AuthProvider.GOOGLE.name());

            User newUser = User
                    .builder()
                    .internalId(userId)
                    .email(email)
                    .firstName(givenName)
                    .lastName(familyName)
                    .from(AuthProvider.GOOGLE)
                    .build();

            saveUser(newUser);

            user = Optional.of(newUser);
        } else {
            log.debug("User with ID " + userId + " from " + AuthProvider.GOOGLE.name() + " already exists");
        }

        return tokenHelper.generateToken(user.get());
    }

    private void saveUser(@RequestBody User userToRegister) {
        if (!StringUtils.isBlank(userToRegister.getPassword())) {
            userToRegister.setPassword(passwordEncoder.encode(userToRegister.getPassword()));
        }

        usersService.save(userToRegister);
    }
}