package musicaround.controller;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import musicaround.model.User;
import musicaround.security.UserAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import static java.util.Optional.ofNullable;

@RestController
@RequestMapping("/v1/auth")
@Slf4j
public class AuthController {

    private static final String BEARER = "Bearer";

    @Autowired
    @NonNull
    private UserAuthenticationService authentication;

    @GetMapping("/logout")
    public void logout(@AuthenticationPrincipal final User user, @RequestParam(value = "everywhere", defaultValue = "false") boolean everywhere, @RequestHeader(value="Authorization") String authorizationHeader) {
        log.debug("Header : " + authorizationHeader);
        if (everywhere) {
            authentication.logout(user);
        } else {
            String token = ofNullable(authorizationHeader)
                    .map(value -> value.replaceFirst(BEARER, ""))
                    .map(String::trim).get();
            authentication.logout(token);
        }
    }
}