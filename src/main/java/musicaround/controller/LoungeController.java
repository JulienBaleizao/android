package musicaround.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.FirebaseMessagingException;
import lombok.extern.slf4j.Slf4j;
import musicaround.common.helpers.FirebaseHelper;
import musicaround.common.helpers.JsonHelper;
import musicaround.common.helpers.NotificationType;
import musicaround.exceptions.http.BadRequestException;
import musicaround.exceptions.http.NoContentException;
import musicaround.lounge.LoungeHelper;
import musicaround.lounge.model.CloseMessage;
import musicaround.model.FirebaseData;
import musicaround.model.Lounge;
import musicaround.model.User;
import musicaround.mqtt.MQTTManager;
import musicaround.service.FavoritesFollowersService;
import musicaround.service.LoungesService;
import musicaround.service.UsersService;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@RestController
@RequestMapping("/v1/lounge")
@Slf4j
public class LoungeController {

    public static final String INTENT_EXTRA_LOUNGE_INFO = "INTENT_EXTRA_LOUNGE_INFO";
    public static final String INTENT_EXTRA_NOTIF_TYPE = "INTENT_EXTRA_NOTIF_TYPE";
    public static final long EXPIRED_DELAY_MS = 180000;

    private List<String> firebaseErrorCode = Arrays.asList("invalid-argument", "registration-token-not-registered");

    @Autowired
    private MQTTManager mqttManager;
    @Autowired
    private LoungesService loungesService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private FavoritesFollowersService favoritesFollowersService;
    @Autowired
    private FirebaseHelper firebaseHelper;

    private Timer timer;
    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

    @PostConstruct
    public void init() {
        // Schedule a process to delete expired lounge
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                deleteExpiredLounges();
            }
        }, 5000, 5000);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public Lounge createLounge(@AuthenticationPrincipal final User user) {

        Lounge lounge = Lounge.builder()
                .token(LoungeHelper.generateToken())
                .topic(mqttManager.generateTopicForLounge())
                .user(user.anonymized())
                .creationDate(System.currentTimeMillis())
                .nbListening(0)
                .lastUpdate(System.currentTimeMillis())
                .build();

        loungesService.save(lounge);

        // Notify followers without impacting the main process
        executor.submit(() -> {
            try {
                onCreateLoungeNotifyFollowers(user, lounge);
            } catch (Exception e) {
                log.error("Fail to send Firebase message", e);
            }
        });

        return lounge;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public Lounge updateLounge(@AuthenticationPrincipal final User user, @RequestBody Lounge loungeToUpdate) {
        Optional<Lounge> lounge = loungesService.findById(loungeToUpdate.get_id());

        if (!lounge.isPresent()) {
            log.error("Fail to update lounge: id not found");
            throw new BadRequestException("Bad input");
        }

        lounge.get().setNbListening(loungeToUpdate.getNbListening());
        lounge.get().setTrack(loungeToUpdate.getTrack());
        lounge.get().setLastUpdate(System.currentTimeMillis());

        loungesService.save(lounge.get());

        return lounge.get();
    }

    @RequestMapping(value = "/{loungeId}", method = RequestMethod.DELETE)
    public void deleteLounge(@AuthenticationPrincipal final User user, @PathVariable String loungeId) {
        if (StringUtils.isBlank(loungeId)) {
            throw new BadRequestException("Bad input");
        }

        loungesService.deleteById(loungeId);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Lounge findAllByLoggedUser(@AuthenticationPrincipal final User user) {
        Optional<Lounge> lounge = loungesService.findByUserInternalId(user.getInternalId());

        return lounge.isPresent() ? lounge.get() : null;
    }

    @RequestMapping(value = "/user/{username}", method = RequestMethod.GET)
    public List<Lounge> findAllByUsername(@AuthenticationPrincipal final User user, @PathVariable String username, @RequestParam("page") int page, @RequestParam("size") int size) {
        List<Lounge> lounges = loungesService.findByUsernameIsLike(username, PageRequest.of(page, size));

        return lounges;
    }

    @RequestMapping(value = "/{loungeId}", method = RequestMethod.GET)
    public Lounge findByLoungeId(@AuthenticationPrincipal final User user, @PathVariable String loungeId) {
        Optional<Lounge> lounge = loungesService.findById(loungeId);

        if (lounge.isPresent()) {
            return lounge.get();
        }

        throw new NoContentException("Lounge not found");
    }

    @RequestMapping(value = "/latest", method = RequestMethod.GET)
    public List<Lounge> findAllMoreRecent(@AuthenticationPrincipal final User user, @RequestParam("page") int page, @RequestParam("size") int size) {
        Page<Lounge> loungePage = loungesService.findAllOrderByCreationDateAsc(PageRequest.of(page, size));

        return loungePage.getContent();
    }

    @RequestMapping(value = "/top", method = RequestMethod.GET)
    public List<Lounge> findAllTop(@AuthenticationPrincipal final User user, @RequestParam("page") int page, @RequestParam("size") int size) {
        Page<Lounge> loungePage = loungesService.findAllByOrderByNbListeningDesc(PageRequest.of(page, size));

        return loungePage.getContent();
    }

    private void onCreateLoungeNotifyFollowers(User user, Lounge lounge) {
        favoritesFollowersService.findByUserFollowed(user.getInternalId()).ifPresent(ff -> {
            ff.getUsersFollowers().stream().forEach(userId -> {
                usersService.findByInternalId(userId).ifPresent(u -> {
                    u.getFirebaseDatas().stream().forEach(f -> {
                        sendFirebaseMessage(user, lounge, u, f);
                    });
                });
            });
        });
    }

    private void sendFirebaseMessage(User user, Lounge lounge, User u, FirebaseData f) {
        if (firebaseHelper == null) {
            log.error("Fail to send Firebase message: firebase instance is null");
            return;
        }

        Map<String, String> data = new HashMap<>();
        try {
            data.put(INTENT_EXTRA_LOUNGE_INFO, JsonHelper.OBJECT_MAPPER.writeValueAsString(lounge));
            data.put(INTENT_EXTRA_NOTIF_TYPE, NotificationType.LOUNGE_CREATION.name());
        } catch (JsonProcessingException e) {
            log.error("Fail to serialize to JSON", e);
            return;
        }

        Map<String, User> tokenToRemove = new HashMap<>();

        try {
            firebaseHelper.sendMessage(f.getToken(),
                    data,
                    "Favorite is sharing",
                    user.getUsername() + " is sharing, go listen together",
                    Arrays.asList(user.getUsername()), AndroidConfig.Priority.HIGH,
                    user.getUsername(),
                    null);
        } catch (FirebaseMessagingException e) {
            log.error("Fail to send Firebase message [" + e.getErrorCode() + "]", e);
            if (firebaseErrorCode.contains(e.getErrorCode())) { // token no more valid
                tokenToRemove.put(f.getToken(), u);
            }
        }

        tokenToRemove.forEach((t, u1) -> {
            removeInvalidFirebaseTokens(u1, Arrays.asList(t));
        });
    }

    private void removeInvalidFirebaseTokens(User u, List<String> tokens) {
        log.debug("Removing Firebase token for user " + u.getUsername() + ": " + tokens);

        List<FirebaseData> firebaseDataClone = new ArrayList<>(u.getFirebaseDatas());
        int index = -1;

        for (FirebaseData firebaseData : u.getFirebaseDatas()) {
            index++;
            if (tokens.contains(firebaseData.getToken())) {
                firebaseDataClone.remove(index);
            }
        }

        u.setFirebaseDatas(firebaseDataClone);
        usersService.save(u);
    }

    private void deleteExpiredLounges() {
        List<Lounge> lounges = loungesService.deleteExpiredLounge();
        if (lounges.size() > 0)
            log.debug(lounges.size() + " expired lounge deleted");

        // Notify listeners that lounge is closed
        lounges.forEach(l -> {
            try {
                mqttManager.publish(l.getTopic(), JsonHelper.GSON.toJson(new CloseMessage()));
            } catch (MqttException e) {
                e.printStackTrace();
            }
        });
    }
}