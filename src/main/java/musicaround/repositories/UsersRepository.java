package musicaround.repositories;

import musicaround.model.User;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends MongoRepository<User, String> {

    Optional<User> findBy_id(ObjectId _id);

    Optional<User> findByInternalId(String internalId);

    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameIgnoreCase(String username);

    Optional<User> findByEmail(String email);

    List<User> findByUsernameIsLikeIgnoreCase(String username, Pageable pageable);

    List<User> findByInternalIdIn(List<String> internalIds, Pageable pageable);
}
