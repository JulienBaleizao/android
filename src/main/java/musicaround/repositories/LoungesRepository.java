package musicaround.repositories;

import musicaround.model.Lounge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface LoungesRepository extends MongoRepository<Lounge, String> {

    Optional<Lounge> findByUserInternalId(String userInternalId);

    List<Lounge> findByUserUsernameIsLikeIgnoreCase(String username, Pageable pageable);

    Page<Lounge> findAllByOrderByCreationDateDesc(Pageable pageable);

    Page<Lounge> findAllByOrderByNbListeningDesc(Pageable pageable);

    Page<Lounge> findByLastUpdateIsLessThan(long lessThan, Pageable pageable);

    int deleteByLastUpdateIsLessThan(long lessThan);
}
