package musicaround.repositories;

import musicaround.model.Token;
import musicaround.model.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface TokensRepository extends MongoRepository<Token, String> {

    Optional<Token> findBy_id(ObjectId _id);

    Optional<Token> findByToken(String token);

    List<Token> findByUserId(String userId);

    Optional<Token> findByUserIdAndValid(String userId, boolean valid);
}
