package musicaround.repositories;

import musicaround.model.FavoritesFollowers;
import musicaround.model.Lounge;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface FavoritesFollowersRepository extends MongoRepository<FavoritesFollowers, String> {

    Optional<FavoritesFollowers> findByUserFollowed(String userId);
}
