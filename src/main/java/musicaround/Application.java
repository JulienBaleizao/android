package musicaround;

import musicaround.common.helpers.FirebaseHelper;
import musicaround.config.YAMLConfig;
import musicaround.mqtt.MQTTManager;
import musicaround.security.TokenHelper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.YamlMapFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    MQTTManager mqttManager(YAMLConfig yamlConfig) {
        return new MQTTManager(yamlConfig).connect();
    }

    @Bean
    YAMLConfig yamlConfig() {
        return new YAMLConfig();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public FirebaseHelper firebaseHelper(YAMLConfig yamlConfig) throws IOException {
        return new FirebaseHelper().init(yamlConfig);
    }
}
