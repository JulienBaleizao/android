package musicaround.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data

public class Track {

    private Artist artist;
    private List<Artist> artists;
    private Album album;
    private long duration;
    private String name;
    private String uri;
    private String imageUri;

    @JsonCreator
    public Track(
            @JsonProperty("artist") final Artist artist,
            @JsonProperty("artists") final List<Artist> artists,
            @JsonProperty("album") final Album album,
            @JsonProperty("duration") final long duration,
            @JsonProperty("name") final String name,
            @JsonProperty("uri") final String uri,
            @JsonProperty("imageUri") final String imageUri
    ) {
        super();
        this.artist = artist;
        this.artists = artists;
        this.album = album;
        this.duration = duration;
        this.name = name;
        this.uri = uri;
        this.imageUri = imageUri;
    }
}
