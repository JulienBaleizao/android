package musicaround.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import musicaround.common.AuthProvider;
import musicaround.common.helpers.JsonHelper;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Objects.requireNonNull;


@Document(collection = "users")
@Builder
public class User implements UserDetails {
    private static final long serialVersionUID = 2396654715019746670L;

    @Id
    private ObjectId _id;
    private String internalId;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;
    private String countryIsoCode;
    private AuthProvider from;
    private List<String> favorites = new ArrayList<>();
    private List<FirebaseData> firebaseDatas = new ArrayList<>();

    public User() {
    }

    @JsonCreator
    User(@JsonProperty("_id") final ObjectId _id,
         @JsonProperty("internalId") final String internalId,
         @JsonProperty("firstName") final String firstName,
         @JsonProperty("lastName") final String lastName,
         @JsonProperty("username") final String username,
         @JsonProperty("password") final String password,
         @JsonProperty("email") final String email,
         @JsonProperty("countryIsoCode") final String countryIsoCode,
         @JsonProperty("from") final AuthProvider from,
         @JsonProperty("favorites") final List<String> favorites,
         @JsonProperty("firebaseDatas") final List<FirebaseData> firebaseDatas) {
        super();
        this._id = _id;
        this.internalId = internalId;
        this.username = username;
        this.password = password;
        this.email = requireNonNull(email);
        this.firstName = firstName;
        this.lastName = lastName;
        this.from = from;
        this.favorites = favorites == null ? new ArrayList<>() : favorites;
        this.firebaseDatas = firebaseDatas;
        this.countryIsoCode = countryIsoCode;
    }

    public String get_id() {
        if (_id == null) {
            return null;
        }
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    @JsonIgnore
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public AuthProvider getFrom() {
        return from;
    }

    public void setFrom(AuthProvider from) {
        this.from = from;
    }

    public List<String> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<String> favorites) {
        this.favorites = favorites == null ? new ArrayList<>(): favorites;
    }

    public List<FirebaseData> getFirebaseDatas() {
        return firebaseDatas;
    }

    public void setFirebaseDatas(List<FirebaseData> firebaseDatas) {
        this.firebaseDatas = firebaseDatas == null ? new ArrayList<>(): firebaseDatas;
    }

    public String getCountryIsoCode() {
        return countryIsoCode;
    }

    public void setCountryIsoCode(String countryIsoCode) {
        this.countryIsoCode = countryIsoCode;
    }

    public User anonymized() {
        User userCopy = JsonHelper.GSON.fromJson(JsonHelper.GSON.toJson(this), User.class);

        userCopy.setFirebaseDatas(new ArrayList<>());
        userCopy.setFavorites(new ArrayList<>());
        userCopy.setPassword(null);
        userCopy.setFrom(null);

        return userCopy;
    }

    @Override
    public String toString() {
        return "User{" +
                "_id=" + _id +
                ", internalId='" + internalId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", from=" + from +
                ", firebaseDatas=" + firebaseDatas +
                ", favorites=" + favorites +
                '}';
    }
}