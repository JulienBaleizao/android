package musicaround.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Album {

    private String name;
    private String uri;

    @JsonCreator
    public Album(
            @JsonProperty("name") final String name,
            @JsonProperty("uri") final String uri
    ) {
        super();
        this.name = name;
        this.uri = uri;
    }
}
