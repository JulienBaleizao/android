package musicaround.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class FirebaseData {

    private SmartphonePlatform smartphonePlatform;
    private String token;

    @JsonCreator
    public FirebaseData(
            @JsonProperty("smartphonePlatform") final SmartphonePlatform smartphonePlatform,
            @JsonProperty("token") final String token
    ) {
        super();
        this.smartphonePlatform = smartphonePlatform;
        this.token = token;
    }

    public SmartphonePlatform getSmartphonePlatform() {
        return smartphonePlatform;
    }

    public void setSmartphonePlatform(SmartphonePlatform smartphonePlatform) {
        this.smartphonePlatform = smartphonePlatform;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
