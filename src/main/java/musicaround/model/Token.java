package musicaround.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tokens")
@Builder
public class Token {

    @Id
    private ObjectId _id;
    private String token;
    private String refreshToken;
    private Long expireIn;
    private String userId;
    private boolean valid;

    @JsonCreator
    public Token(@JsonProperty("_id") ObjectId _id,
                 @JsonProperty("token") String token,
                 @JsonProperty("refreshToken") String refreshToken,
                 @JsonProperty("expireIn") Long expireIn,
                 @JsonProperty("userId") String userId,
                 @JsonProperty("valid") boolean valid) {
        this._id = _id;
        this.token = token;
        this.refreshToken = refreshToken;
        this.expireIn = expireIn;
        this.userId = userId;
        this.valid = valid;
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(Long expireIn) {
        this.expireIn = expireIn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
