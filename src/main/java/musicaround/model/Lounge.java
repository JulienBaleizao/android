package musicaround.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "lounges")
@Builder
public class Lounge {

    @Id
    private ObjectId _id;
    private String token;
    private String topic;
    private User user;
    private Long creationDate;
    private long nbListening;
    private Track track;
    private Long lastUpdate;

    @JsonCreator
    public Lounge(
            @JsonProperty("_id") final ObjectId _id,
            @JsonProperty("token") final String token,
            @JsonProperty("topic") final String topic,
            @JsonProperty("user") final User user,
            @JsonProperty("creationDate") final Long creationDate,
            @JsonProperty("nbListening") final long nbListening,
            @JsonProperty("track") final Track track,
            @JsonProperty("lastUpdate") final Long lastUpdate
    ) {
        super();
        this._id = _id;
        this.token = token;
        this.topic = topic;
        this.user = user;
        this.creationDate = creationDate;
        this.nbListening = nbListening;
        this.track = track;
        this.lastUpdate = lastUpdate;
    }

    public String get_id() {
        if (_id == null) {
            return null;
        }
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public long getNbListening() {
        return nbListening;
    }

    public void setNbListening(long nbListening) {
        this.nbListening = nbListening;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}