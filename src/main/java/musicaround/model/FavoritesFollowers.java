package musicaround.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Document(collection = "favorites_followers")
@Builder
public class FavoritesFollowers {

    @Id
    private ObjectId _id;
    private String userFollowed;
    private Set<String> usersFollowers;

    @JsonCreator
    public FavoritesFollowers(
            @JsonProperty("_id") final ObjectId _id,
            @JsonProperty("userFollowed") final String userFollowed,
            @JsonProperty("usersFollowers") final Set<String> usersFollowers
    ) {
        super();
        this._id = _id;
        this.userFollowed = userFollowed;
        this.usersFollowers = usersFollowers == null ? new HashSet<>() : usersFollowers;
    }

    public String get_id() {
        if (_id == null) {
            return null;
        }
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getUserFollowed() {
        return userFollowed;
    }

    public void setUserFollowed(String userFollowed) {
        this.userFollowed = userFollowed;
    }

    public Set<String> getUsersFollowers() {
        return usersFollowers;
    }

    public void setUsersFollowers(Set<String> usersFollowers) {
        this.usersFollowers = usersFollowers;
    }

    public void addUsersFollowers(String usersFollower) {
        this.usersFollowers.add(usersFollower);
    }

    public void removeUsersFollowers(String usersFollower) {
        this.usersFollowers.remove(usersFollower);
    }

    @Override
    public String toString() {
        return "FavoritesFollowers{" +
                "_id=" + _id +
                ", userFollowed='" + userFollowed + '\'' +
                ", usersFollowers=" + usersFollowers +
                '}';
    }
}